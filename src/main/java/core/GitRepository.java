package core;

import java.io.*;

public class GitRepository {

    String repo_path;

    public GitRepository(String path){
        repo_path = path;
    }

    public String getHeadRef() throws FileNotFoundException, IOException {
        BufferedReader bf = new BufferedReader(new FileReader(repo_path + "/HEAD"));
        String headref = bf. readLine().substring(5);
        return headref;
    }

    public String getRefHash(String s) throws FileNotFoundException, IOException{
        BufferedReader bf = new BufferedReader(new FileReader(repo_path+ "/" + s));
        return bf.readLine();
    }
}
