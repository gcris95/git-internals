package core;

import java.io.*;
import java.util.zip.InflaterInputStream;

public class GitBlobObject {

    String path;
    String hash;

    public GitBlobObject(String path, String hash){
        this.path = path;
        this.hash = hash;
    }

    public String getType() throws IOException {
        InflaterInputStream inflate = new InflaterInputStream(new FileInputStream(path+"/objects/"+hash.substring(0,2)+"/"+hash.substring(2)));
        Reader reader = new InputStreamReader(inflate);
        BufferedReader br = new BufferedReader(reader);

        return br.readLine().substring(0,4);
    }

    public String getContent() throws IOException {

        InflaterInputStream inflate = new InflaterInputStream(new FileInputStream(path+"/objects/"+hash.substring(0,2)+"/"+hash.substring(2)));
        Reader reader = new BufferedReader(new InputStreamReader(inflate));

        String dirtyContent ="";

        int r;
        char c;
        while ((r = reader.read()) != -1) {
            c = (char) r;
            dirtyContent += c;
        }

        return dirtyContent.substring(dirtyContent.indexOf("\0")+1);

    }

}
